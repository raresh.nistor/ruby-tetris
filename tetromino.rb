class Tetromino
    attr_reader :blocks, :color

    def initialize(blocks, color)
        @blocks = blocks
        @color = color
    end

    def [](x, y)
        puts x, y
    end

    def self.tetrominoes
        def i
            Tetromino.new([
            ])
        end

        def o
        end

        def t
        end

        def s
        end

        def z
        end

        def j
        end

        def l
        end

        return [i, o, t, s, z, j, l]
    end
end
